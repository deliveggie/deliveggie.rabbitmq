﻿using System;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;
using EasyNetQ;

namespace DeliVeggie.RabbitMq
{
    public class Subscriber : ISubscriber
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly IBus _bus;
        private bool _disposed;

        public Subscriber(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;

            _bus = _connectionProvider.GetConnection();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _bus?.Dispose();

            _disposed = true;
        }

        public void Subscribe(Func<RequestMessage, IResponseMessage> callback)
        {
            _bus.Rpc.Respond(callback);
        }
    }
}