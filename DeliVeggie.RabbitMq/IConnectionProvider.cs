﻿using System;
using EasyNetQ;

namespace DeliVeggie.RabbitMq
{
    public interface IConnectionProvider : IDisposable
    {
        IBus GetConnection();
    }
}