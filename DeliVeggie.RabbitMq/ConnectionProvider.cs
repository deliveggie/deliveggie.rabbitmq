﻿using System;
using EasyNetQ;

namespace DeliVeggie.RabbitMq
{
    public class ConnectionProvider : IConnectionProvider
    {
        private readonly IBus _bus;
        private bool _disposed;

        public ConnectionProvider(string connectionString)
        {
            _bus = RabbitHutch.CreateBus(connectionString);
        }
        
        public IBus GetConnection()
        {
            return _bus;
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _bus?.Dispose();

            _disposed = true;
        }
    }
}