﻿using System;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;
using EasyNetQ;

namespace DeliVeggie.RabbitMq
{
    public class Publisher : IPublisher
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly IBus _bus;
        private bool _disposed;

        public Publisher(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
            _bus = _connectionProvider.GetConnection();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _bus?.Dispose();

            _disposed = true;
        }
        
        public IResponseMessage Publish(RequestMessage request)
        {
            return _bus.Rpc.Request<RequestMessage, IResponseMessage>(request);
        }
    }
}