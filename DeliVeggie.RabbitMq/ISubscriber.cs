﻿using System;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;

namespace DeliVeggie.RabbitMq
{
    public interface ISubscriber : IDisposable
    {
        void Subscribe(Func<RequestMessage, IResponseMessage> callback);
    }
}