﻿using System;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;

namespace DeliVeggie.RabbitMq
{
    public interface IPublisher : IDisposable
    {
       IResponseMessage Publish(RequestMessage request);
    }
}